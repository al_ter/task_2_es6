import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (!fighter) return;
  
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);

  fighterElement.append(fighterImage, fighterInfo);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

const createFighterInfo = fighter => {
  const infoAttributes = [
    'name',
    'health',
    'attack',
    'defense'
  ];

  const fighterInfoElement = createElement({
    tagName: 'div',
    className: `fighter-preview___info`
  });

  fighterInfoElement.innerHTML = infoAttributes
    .map(attr => `<span><strong>${attr}</strong>: ${fighter[attr]}</span>`)
    .join('');

  return fighterInfoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
