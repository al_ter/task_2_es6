import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;
  const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
  const secondFighterHealthBar = document.getElementById('right-fighter-indicator');
  let winner;

  const pressedKeysMap = new Map();

  // Gathering all possible keys to Map and setting their default values to false
  Object.values(controls)
    .map(value => {
      if (value instanceof Array) {
        value.map(innerValue => pressedKeysMap.set(innerValue, false));
        return false;
      }
      pressedKeysMap.set(value, false);
  });

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const update = (fighter, damage) => {
      const healthElement = fighter === firstFighter ? firstFighterHealthBar : secondFighterHealthBar;
      fighter.currentHealth = Math.max(0, fighter.currentHealth - damage);
      healthElement.style.width = `${fighter.currentHealth / fighter.health * 100}%`;
      


      if (fighter.currentHealth === 0) {
        winner = fighter === firstFighter ? secondFighter : firstFighter;
        resolve(winner);
      }
    }

    let firstFighterCanCrit = true;
    let secondFighterCanCrit = true;

    const fightController = keysMap => {
      if (
        keysMap.get(controls.PlayerOneCriticalHitCombination[0]) &&
        keysMap.get(controls.PlayerOneCriticalHitCombination[1]) &&
        keysMap.get(controls.PlayerOneCriticalHitCombination[2]) &&
        firstFighterCanCrit
      ) {
        // first player crit
        update(secondFighter, getDamage(firstFighter, secondFighter, true));
        firstFighterCanCrit = false;
        setTimeout(() => firstFighterCanCrit = true, 10000);
      } else if (
        keysMap.get(controls.PlayerTwoCriticalHitCombination[0]) &&
        keysMap.get(controls.PlayerTwoCriticalHitCombination[1]) &&
        keysMap.get(controls.PlayerTwoCriticalHitCombination[2]) &&
        secondFighterCanCrit
      ) {
        // second player crit
        update(firstFighter, getDamage(secondFighter, firstFighter, true));
        secondFighterCanCrit = false;
        setTimeout(() => secondFighterCanCrit = true, 10000);
      } else if (
        keysMap.get(controls.PlayerOneAttack) &&
        !keysMap.get(controls.PlayerOneBlock) &&
        keysMap.get(controls.PlayerTwoBlock)
      ) {
        // first hit, seconde defend
        update(secondFighter, 0);
      } else if (
        keysMap.get(controls.PlayerTwoAttack) &&
        !keysMap.get(controls.PlayerTwoBlock) &&
        keysMap.get(controls.PlayerOneBlock)
      ) {
        // second hit, first defend
        update(firstFighter, 0);
      } else if (
        keysMap.get(controls.PlayerOneAttack) &&
        !keysMap.get(controls.PlayerOneBlock)
      ) {
        // first hit
        update(secondFighter, getDamage(firstFighter, secondFighter));
      } else if (
        keysMap.get(controls.PlayerTwoAttack) &&
        !keysMap.get(controls.PlayerTwoBlock)
      ) {
        // second hit
        update(firstFighter, getDamage(secondFighter, firstFighter));
      }
    }

    const keydownHandler = e => {
      if (!pressedKeysMap.has(e.code) || e.repeat) {
        return;
      }

      pressedKeysMap.set(e.code, true);
      fightController(pressedKeysMap);
    }

    const keyupHandler = ({ code }) => pressedKeysMap.set(code, false);

    document.addEventListener('keydown', keydownHandler, false);
    document.addEventListener('keyup', keyupHandler, false);
  });
}

export function getDamage(attacker, defender = null, isCrit = false) {
  let damage = 0;

  if (isCrit) {
    damage = getHitPower(attacker, true);
  } else if (!defender) {
    damage = getHitPower(attacker);
  } else {
    damage = getHitPower(attacker) - getBlockPower(defender);
  }
  console.log(`DAMAGE: ${Math.max(0, damage)}`);
  return Math.max(0, damage);
}

export function getHitPower(fighter, isCrit = false) {
  const { attack } = fighter;
  const criticalHitChance = getRandom(1, 2);
  const hitPower = isCrit ? attack * 2 : attack * criticalHitChance;
  console.log(`${fighter.name} hits: ${hitPower}`);
  return hitPower;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = getRandom(1, 2);
  const blockPower = defense * dodgeChance;
  console.log(`${fighter.name} blocks: ${blockPower}`);
  return blockPower;
}

const getRandom = (min, max) => (Math.random() * (max - min) + min);
