import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const fighterImage = createFighterImage(fighter);
  const onClose = () => document.location.reload();

  showModal({
    title: `${fighter.name} WINS!`,
    bodyElement: fighterImage,
    onClose
  });
}
